//
//  AggregatedAnalyticsManager.swift
//  
//
//  Created by Alexei Muraveinik on 21.10.22.
//

import AppTrackingTransparency

public final class AggregatedAnalyticsManager {
    private var managers = [AnalyticsManager]()
    
    public init() {}
    
    public func add(_ manager: AnalyticsManager) {
        managers.append(manager)
    }
    
    public func remove(_ manager: AnalyticsManager) {
        managers.removeAll(where: { $0.id == manager.id })
    }
}


// MARK: AnalyticsManager
extension AggregatedAnalyticsManager: AnalyticsManager {
    public func identify(user: String, domain: String?) {
        for manager in managers {
            manager.identify(user: user, domain: domain)
        }
    }
    
    public func logEvent(eventName: String, params: [String: String]) {
        for manager in managers {
            manager.logEvent(eventName: eventName, params: params)
        }
    }
    
    public func getIdentityParams() -> [String: String] {
        managers
            .map { $0.getIdentityParams() }
            .reduce(into: [:]) { result, dict in
                result.merge(dict, uniquingKeysWith: { old, _ in old })
            }
    }
    
    public func start() {
        for manager in managers {
            manager.start()
        }
        
        if #available(iOS 14, *) {
            if ATTrackingManager.trackingAuthorizationStatus == .authorized {
                return
            }
            
            ATTrackingManager.requestTrackingAuthorization { status in
                switch status {
                case .notDetermined:
                    print(attTag, "Not determined")
                case .restricted:
                    print(attTag, "Restricted")
                case .denied:
                    print(attTag, "Denied")
                case .authorized:
                    print(attTag, "Authorized")
                @unknown default:
                    print(attTag, "Unknown default")
                }
            }
        }
    }
}


private let attTag = "[ATTrackingManager.requestTrackingAuthorization]: "
