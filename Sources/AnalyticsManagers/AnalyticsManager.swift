//
//  AnalyticsManager.swift
//  
//
//  Created by Alexei Muraveinik on 21.10.22.
//

public enum AnalyticsEvent: String {
    case login
}

public protocol AnalyticsManager {
    var id: Self.Type { get }
    
    func identify(user: String, domain: String?)
    func logEvent(eventName: String, params: [String: String])
    func getIdentityParams() -> [String: String]
    
    func start()
}


public extension AnalyticsManager {
    var id: Self.Type { Self.self }
    
    func logEvent(eventName: String) {
        logEvent(eventName: eventName, params: [:])
    }
    
    func logEvent(_ event: AnalyticsEvent, params: [String: String]) {
        logEvent(eventName: event.rawValue, params: params)
    }
    
    func logEvent(_ event: AnalyticsEvent) {
        logEvent(event, params: [:])
    }
    
    func getIdentityParams() -> [String: String] {
        [:]
    }
}
