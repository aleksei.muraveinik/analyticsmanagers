//
//  DebugAnalyticsManager.swift
//  
//
//  Created by Alexei Muraveinik on 21.10.22.
//

public final class DebugAnalyticsManager {
    public init() {
        log("Analytics initialised")
    }
    
    private func serializeParams(params: [String: Any]) -> String {
        params
            .map { "\($0)=\"\($1)\"" }
            .joined(separator: ", ")
    }

    private func log(_ message: String) {
        print(TAG, message)
    }
}


// MARK: AnalyticsManager
extension DebugAnalyticsManager: AnalyticsManager {
    public func identify(user: String, domain: String?) {
        log("identify called with domain='\(String(describing: domain))', user='\(String(describing: user))'")
    }
    
    public func logEvent(eventName: String, params: [String: String]) {
        let pars = params.isEmpty
            ? "no params"
            : "params={\(serializeParams(params: params))}"
        
        log("Tracking event='\(eventName)', with \(pars)")
    }
    
    public func start() {}
}

private let TAG = "ANM"
